package com.charles;

import java.io.IOException;
import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ch8_7 {
    public static void firstHundredWords() {
        // Use War and Peace for testing
        try (Stream<String> words = ch8.warAndPeaceTokens()) {
            // List the first 100 words
            words.filter(s -> ch8_6.isWord(s)).limit(100).forEach(System.out::println);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void tenMostFrequentWords() {
        try (Stream<String> words = ch8.warAndPeace().stream()) {//ch8.warAndPeaceTokens()) {
            Map<String, Long> wordHist = words.map(s -> s.toLowerCase()).collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

            // Sort the resulting map
            List<Entry<String, Long>> entrySet = new ArrayList<>(wordHist.entrySet());
            entrySet.sort(Entry.comparingByValue(Comparator.reverseOrder()));
            Map<String, Long> result = new LinkedHashMap<>();
            for (Entry<String, Long> entry : entrySet) {
                result.put(entry.getKey(), entry.getValue());
            }
            //

            // Print the first 10
            Iterator k = result.keySet().iterator();
            for (int i = 0; i < 10; i++) {
                Object o = k.next();
                System.out.println(o + " : " + result.get(o));
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
