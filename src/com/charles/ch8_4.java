package com.charles;

import java.util.stream.Stream;

public class ch8_4 {
    public static Stream<Long> randomNumbers(long seed) {
        final long c = 11L;
        final long a = 2521403917L;
        final long m = (long) Math.pow(2, 48);
        return Stream.iterate(seed, n -> (a*n+c)%m);
    }
}
