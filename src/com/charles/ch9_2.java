package com.charles;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ch9_2 {

    // Write a program that reads a text file and produces a file with the same
    // name but extension .toc, containing an alphabetized list of all words in
    // the input file together with a list of line numbers in which each word occurs.
    // Assume that the file's encoding is UTF-8.
    // Horstmann, Cay S.. Core Java SE 9 for the Impatient (Kindle Locations 10864-10866).
    // Pearson Education. Kindle Edition.

    private static Map<String, Set<Integer>> word2LinesMap = new TreeMap<>();

    public static void alphabetizeFile() {
        Path warAndPeacePath = Paths.get("misc/war_and_peace.txt");
        List<String> lines = null;

        try {
            lines = Files.readAllLines(warAndPeacePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int currentLineNumber = 0;

        for (String line : lines) {

            // Get all words on the line
            List<String> lineWords = List.of(line.split("\\PL+"));

            for (String s : lineWords) {

                Set<Integer> linesWordOccursOn = word2LinesMap.get(s);

                if (linesWordOccursOn == null)
                    linesWordOccursOn = new TreeSet<>();

                linesWordOccursOn.add(currentLineNumber);
                word2LinesMap.put(s, linesWordOccursOn);
            }

            currentLineNumber++;
        }

        // Test
        for (String key : word2LinesMap.keySet()) {
            System.out.println("Word : " + key + " Lines : " + word2LinesMap.get(key));
        }

        // Okay this piece of shit counts as solved for now
    }

    public static void main(String[] args) {
        alphabetizeFile();
    }
}
