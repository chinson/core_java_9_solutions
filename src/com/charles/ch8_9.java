package com.charles;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ch8_9 {
    /* Read words from /usr/share/dict/words into a stream and produce an array
        of all words containing five distinct values */
    public static void wordsContainingFiveDistinctVowels() {
        try (Stream<String> words = ch8.dictWords().stream()) {
            List<String> vowels = toStringList("aeiou");
            //vowels.stream().forEach(System.out::println);
            words.map(String::toLowerCase).filter(s -> toStringList(s).containsAll(vowels)).forEach(System.out::println);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static List<String> toStringList(String s) {
        char[] chars =  s.toCharArray();
        List<String> ans = new ArrayList<>();
        for (char c : chars) {
            ans.add(Character.toString(c));
        }
        return ans;
    }
}