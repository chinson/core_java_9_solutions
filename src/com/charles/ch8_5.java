package com.charles;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ch8_5 {
    public static Stream<String> ch8CodePoints(String s) {
        // original version
//		List<String> result = new ArrayList<>();
//		int i = 0;
//		while (i < s.length()) {
//			int j = s.offsetByCodePoints(i, 1);
//			result.add(s.substring(i, j));
//			i = j;
//		}
//		return result.stream();

        // Make finite stream of offsets and extract
        return IntStream.iterate(0, i -> i < s.length(), i -> i = s.offsetByCodePoints(i, 1)).mapToObj(i -> s.substring(i, s.offsetByCodePoints(i, 1)));
    }

}
