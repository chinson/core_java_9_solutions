package com.charles;

import java.util.DoubleSummaryStatistics;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ch8_10 {
    /* given a finite stream of strings, find the average string length*/
    public static void averageStringLength(Stream<String> strings) {
        DoubleSummaryStatistics summary = strings.collect(Collectors.summarizingDouble(String::length));
        System.out.println("Average word length : " + summary.getAverage());
    }
}
