package com.charles;

import java.io.IOException;
import java.util.List;

import static com.charles.ch8.*;

public class ch8_2 {

    public static void compareStreamSpeed() {
        try {
            List<String> words = warAndPeace();

            long start = System.currentTimeMillis();
            words.stream().filter(w -> w.length() > 12).count();
            long finish = System.currentTimeMillis();
            long timeMs = finish - start;
            System.out.println("Normal call : " + timeMs);

            start = System.currentTimeMillis();
            words.parallelStream().filter(w -> w.length() > 12).count();
            finish = System.currentTimeMillis();
            timeMs = finish - start;
            System.out.println("Parallel call : " + timeMs);

        } catch (IOException ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }

}
