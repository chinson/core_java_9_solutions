package com.charles;

import java.util.ArrayList;
import java.util.function.Function;
import java.util.stream.Stream;

public class ch8_14 {
    public static <T> ArrayList<T> streamOfArraylistToArraylist(Stream<ArrayList<T>> arrayListStream) {
//        ArrayList<T> result = new ArrayList<>();
//        arrayListStream.forEach(t -> result.addAll(t));
//        return result;

        ArrayList<T> result = arrayListStream.reduce((x, y) -> {x.addAll(y); return x;} ).get();
        return result;
    }
}
