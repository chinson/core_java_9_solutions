package com.charles;

import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Stream;

public class Charles {
    public static void main(String[] args) {
        ArrayList<Integer> test1 = new ArrayList<>();
        test1.add(1);
        test1.add(2);
        test1.add(3);
        test1.add(4);
        test1.add(5);

        ArrayList<Integer> test2 = new ArrayList<>();
        test2.add(10);
        test2.add(11);

        //ch8_13.zip(test2.stream(), test1.stream()).forEach(System.out::println);
        Stream<ArrayList<Integer>> testStream = Stream.of(test1, test2);
        ch8_14.streamOfArraylistToArraylist(testStream).stream().forEach(System.out::println);
    }
}
