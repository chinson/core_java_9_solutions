package com.charles;


import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.*;
import java.nio.file.FileSystem.*;
import java.util.Collections;

public class ch9_8 {
    // Produce a zip file of a directory and its descendants
    public static void zipup (String sourceDirPath, String targetDirPath) {
        Path zipPath = Paths.get(targetDirPath + "/file.zip");
        URI uri = null;
        try {
            uri = new URI("jar", zipPath.toUri().toString(), null);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        // Still dunno why I need the "jar" part...
        try (FileSystem zipFs = FileSystems.newFileSystem(uri, Collections.singletonMap("create", "true"))) {
            Path targetPath = zipFs.getPath("/");
            System.out.println(targetPath.toString());
            targetPath = targetPath.resolve(targetDirPath);
            Files.copy(Paths.get(sourceDirPath), targetPath);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    public static void zipup2 (Path zipLoc, Path toBeAdded, String internalPath) {
        URI fileUri = zipLoc.toUri();
        URI zipUri = null;
        try {
            zipUri = new URI("jar" + fileUri.getScheme(), fileUri.getPath(), null);
            System.out.println(zipUri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        try (FileSystem zipFs = FileSystems.newFileSystem(zipUri, Collections.singletonMap("create", "true")) ) {

            Path internalTargetPath = zipFs.getPath(internalPath);
            Files.createDirectories(internalTargetPath.getParent());
            Files.copy(toBeAdded, internalTargetPath, StandardCopyOption.REPLACE_EXISTING);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Lets try this...
    public static void main(String[] args) {
        //zipup("misc", "test");
        Path zipLocation = FileSystems.getDefault().getPath("test/test.zip").toAbsolutePath();
        Path toBeAdded = FileSystems.getDefault().getPath("war_and_peace.txt").toAbsolutePath();
        zipup2(zipLocation, toBeAdded, "misc/war_and_peace.txt");
    }
}
