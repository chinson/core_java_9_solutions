package com.charles;

import java.util.DoubleSummaryStatistics;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ch8_11 {
    /* given a finite stream of strings, find the average string length*/
    public static void maxStringLength(Stream<String> strings) {
        DoubleSummaryStatistics summary = strings.collect(Collectors.summarizingDouble(String::length));
        System.out.println("Max word length : " + summary.getMax());
    }
}
