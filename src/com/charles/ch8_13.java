package com.charles;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class ch8_13 {
    public static <T> Stream<T> zip(Stream<T> first, Stream<T> second) {
        Iterator<T> f = first.iterator();
        Iterator<T> s = second.iterator();

        List<T> zipped = new ArrayList<>();

        while (f.hasNext() && s.hasNext()) {
            zipped.add(f.next());
            zipped.add(s.next());
        }

        while (f.hasNext() && !s.hasNext()) {
            zipped.add(f.next());
            zipped.add(null);
        }

        while (!f.hasNext() && s.hasNext()) {
            zipped.add(null);
            zipped.add(s.next());
        }

        assert (!f.hasNext() && !s.hasNext());
        return zipped.stream();
    }
}
