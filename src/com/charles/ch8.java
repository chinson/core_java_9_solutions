package com.charles;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

/*
    Various methods for chapter 8
 */
public class ch8 {
    public static List<String> dictWords() throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get("/usr/share/dict/words")));
        return List.of(contents.split("\\PL+"));
    }

    // Read the novel "War and Peace" for testing
    public static List<String> warAndPeace() throws IOException {
        String contents = new String(Files.readAllBytes(Paths.get("misc/war_and_peace.txt")));
        return List.of(contents.split("\\PL+")); // Split into words, nonletters are delimiters
    }

    // Careful, this one splits up the words in weird ways sometimes
    public static Stream<String> warAndPeaceTokens() throws IOException {
        Scanner sc = new Scanner(new File("misc/war_and_peace.txt"));
        return sc.tokens();
    }
}
