package com.charles;

public class ch8_6 {
    public static boolean isWord(String s) {
        return s.codePoints().allMatch(i -> Character.isAlphabetic(i));
    }

    public static boolean isJavaIdentifier(String s) {
        return s.codePoints().allMatch(i -> Character.isJavaIdentifierPart(i));
    }
}
