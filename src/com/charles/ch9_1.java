package com.charles;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class ch9_1 {
    // Copy an InputStream to an OutputStream
    public static void inputStreamToOutputStream(InputStream is, OutputStream os) {
        int i;
        try {
            try {
                i = is.read();
                while (i != -1) {
                    os.write(i);
                    i = is.read();
                }
            } finally {
                if (is != null)
                    is.close();
                if (os != null)
                    os.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    // Just use the transferTo Method
    public static void inputStreamToOutputStream2(InputStream is, OutputStream os) {
        try {
            try {
                is.transferTo(os);
            } finally { // Still runs if is.transferTo throws an exception
                if (is != null)
                    is.close(); // close() can throw an exception
                if (os != null)
                    os.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void inputStreamToOutputStreamTmpFile(InputStream is, OutputStream os) {
        // Using tmp files
        try {
            try {
                Path tmp = Files.createTempFile(null, null);
                Files.write(tmp, is.readAllBytes());
                os.write(Files.readAllBytes(tmp));
                Files.deleteIfExists(tmp);
            } finally {
                if (is != null)
                    is.close();
                if (is != null)
                    os.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        test();
    }

    public static void test() {
//        Path warAndPeacePath = Paths.get("misc/war_and_peace.txt");
//        Path testOutPath = Paths.get("test/results.txt");
//        InputStream in = null;
//        OutputStream out = null;


        // Test v1
//        try {
//            in = Files.newInputStream(warAndPeacePath);
//            out = Files.newOutputStream(testOutPath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        inputStreamToOutputStream(in, out);

//        // Test v2
//        try {
//            in = Files.newInputStream(warAndPeacePath);
//            out = Files.newOutputStream(testOutPath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        inputStreamToOutputStream2(in, out);


        // Test v3
//        try {
//            in = Files.newInputStream(warAndPeacePath);
//            out = Files.newOutputStream(testOutPath);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        inputStreamToOutputStreamTmpFile(in, out);
    }
}
