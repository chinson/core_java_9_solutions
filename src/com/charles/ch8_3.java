package com.charles;

import java.util.stream.IntStream;

public class ch8_3 {
    public static void testStreamOf() {
        int[] values = { 1, 4, 9, 16 };
        // Doesnt work Stream<Integer> valuesStream = Stream.of(values);
        IntStream valuesStream = IntStream.of(values); // stream of int
        System.out.println(valuesStream.toString());
    }
}
